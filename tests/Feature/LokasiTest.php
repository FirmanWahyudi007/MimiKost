<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\LokasiKost;

class LokasiTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_lokasi_kost_tersimpan()
    {
        $response = $this->get('/');
        
        $tempat = new LokasiKost();
        
        $tempat->lokasi_tempat = "Jalan A3";
        $tempat->latitude = "-8.2631";
        $tempat->longitude = "113.6536";
        $tempat->path_gambar = 'uploads/alsdkajljdshlas.jpg';
        $tempat->save();

        if(!$tempat){
            $response->assertStatus(400);
        }

        $response->assertStatus(200);
    }
}
