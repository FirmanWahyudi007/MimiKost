<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\LokasiKost;
use App\Models\KamarKost;

class KamarTest extends TestCase
{

    public function test_kamar_menyimpan_data()
    {
        $response = $this->get('/');
        $tempat = new LokasiKost();
        $kamar = new KamarKost();
        
        // Save tempat kost
        $tempat->lokasi_tempat = "Jalan A3";
        $tempat->latitude = "-8.2631";
        $tempat->longitude = "113.6536";
        $tempat->path_gambar = 'uploads/alsdkajljdshlas.jpg';
        $tempat->save();

        // Save kamar kost
        $kamar->nama = "jojla aijljglj oiuasdf";
        $kamar->jumlah = "99";
        $kamar->harga = "129009184";
        $kamar->peraturan = "Ini peraturan";
        $kamar->fasilitas = ["kamar mandi dalam", "kulkas"];
        $kamar->lokasi_kost_id = $tempat->id;
        $kamar->save();

        if(!$kamar){
            $response->assertStatus(400);
        }

        $response->assertStatus(200);
    }
    
}
